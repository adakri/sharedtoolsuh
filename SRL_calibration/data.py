# Copyright 2024 adakri
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json 
import os

# data paths for testing

# Testing with 0 case
CAM_NUM = 24
case = 0
dome_data = {
    "images": [os.path.join("SRL_calibration/data", f"dome/Phantom/{case}", f) 
               for f in os.listdir(os.path.join("SRL_calibration/data", f"dome/Phantom/{case}")) if f.endswith("png")],
    "calibs": "SRL_calibration/data/dome/Phantom/0/agi_calib.xml",
    "opencv_calibs": [{"extrinsics": os.path.join("SRL_calibration/data", f"dome/Phantom/Calibration", str(f), "extrinsics.xml"), "intrinsics": os.path.join("SRL_calibration/data", f"dome/Phantom/Calibration", str(f), "intrinsic.xml")} 
               for f in range(1,25)],
    "mesh": f"SRL_calibration/data/dome/Phantom/mesh_00000{case}.obj"
}

uh_data = {
    "images": [os.path.join("SRL_calibration/data", "uh/calibrationboard/20231116142544654/images/000002", f) for f in os.listdir(os.path.join("SRL_calibration/data", "uh/calibrationboard/20231116142544654/images/000002")) 
               if f.endswith("png") and 'A' in f],
    "calibs": "SRL_calibration/data/uh/calibrationboard/20231116142544654/images/20231103095616887.csd",
    "mesh": f"SRL_calibration/data/uh/calibrationboard/20231116142544654/meshes/20231116142544654.000002.obj"
}

# Check
assert os.path.exists(dome_data['images'][0])
assert os.path.exists(dome_data['calibs'])
assert os.path.exists(dome_data["mesh"])

assert os.path.exists(uh_data['images'][0])
assert os.path.exists(uh_data['calibs'])
assert os.path.exists(uh_data["mesh"])
