# Copyright 2024 adakri
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import cv2
import os
import datetime
import sys
import inspect
import argparse
import multiprocessing
import json
import pickle
import glob
import re
import imageio
import shutil
from itertools import repeat

import utils
# Heavily inspired module from https://github.com/cvlab-epfl/multiview_calib (ommited for now)
# TODO: add monotinicity and extrici parameters estimation (bundle adjustment)
#from SRL_calibration.calibration import (enforce_monotonic_distortion, 
#                                        is_distortion_function_monotonic,
#                                        probe_monotonicity)


# Check for OpenCV version for function (could pose issue in Windows) 
(cv2_major, cv2_minor, _) = cv2.__version__.split(".")
if int(cv2_major)<4:
    raise ImportError("Opencv version 4+ required!")

# Ready images of 
def process_image(filename_image, cfg, debug, debug_folder):
    print("Processing image {} ...".format(filename_image))

    # Read image
    colour = imageio.imread(filename_image)
    if(colour.shape[0] < colour.shape[1]):
        colour = cv2.rotate(imageio.imread(filename_image), cv2.ROTATE_90_COUNTERCLOCKWISE)
    gray = utils.rgb2gray(colour)

    # Find the Aruco board corners
    # Warning changes from after opencv 4.7 (https://stackoverflow.com/questions/74964527/attributeerror-module-cv2-aruco-has-no-attribute-dictionary-get)
    arucoDict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_250)
    arucoParams = cv2.aruco.DetectorParameters()
    # Modify thresholding params for small apriltags
    arucoParams.adaptiveThreshWinSizeStep = 1
    detector = cv2.aruco.ArucoDetector(arucoDict, arucoParams)
    (corners, ids, rejected) = detector.detectMarkers(gray)
    
    # If found, add object points, image points (after refining them)
    if not len(corners)>0:
        return None
    
    ids = ids.flatten()
    imgp = []
	# loop over the detected ArUCo corners
    for (markerCorner, markerID) in zip(corners, ids):
        # extract the marker corners (which are always returned in
        # top-left, top-right, bottom-right, and bottom-left order)
        corners = markerCorner.reshape((4, 2))
        (topLeft, topRight, bottomRight, bottomLeft) = corners
        # convert each of the (x, y)-coordinate pairs to integers
        topRight = (int(topRight[0]), int(topRight[1]))
        bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
        bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
        topLeft = (int(topLeft[0]), int(topLeft[1]))
        # compute and draw the center (x, y)-coordinates of the ArUco
        # marker
        cX = int((topLeft[0] + bottomRight[0]) / 2.0)
        cY = int((topLeft[1] + bottomRight[1]) / 2.0)
        imgp.append([cX, cY])

        if debug:
            # draw the bounding box of the ArUCo detection
            cv2.line(colour, topLeft, topRight, (0, 255, 0), 2)
            cv2.line(colour, topRight, bottomRight, (0, 255, 0), 2)
            cv2.line(colour, bottomRight, bottomLeft, (0, 255, 0), 2)
            cv2.line(colour, bottomLeft, topLeft, (0, 255, 0), 2)
            # Circle the center
            cv2.circle(colour, (cX, cY), 4, (0, 0, 255), -1)
            # draw the ArUco marker ID on the image
            cv2.putText(colour, str(markerID),
                (topLeft[0], topLeft[1] - 15), cv2.FONT_HERSHEY_SIMPLEX,
                0.5, (0, 255, 0), 2)
            print("[INFO] ArUco marker ID: {}".format(markerID))
            # show the output image
    if(debug):
        cv2.imshow("Image", colour)
        cv2.waitKey(0)
        
        cv2.imwrite(os.path.join(debug_folder, os.path.basename(filename_image)), colour)

    return (np.float32(imgp), ids)

def main(folder_images, output_folder, description, 
         config_file, 
         alpha, threads, force_monotonicity, monotonic_range, 
         rational_model, fix_principal_point, fix_aspect_ratio, 
         zero_tangent_dist, criteria_eps, 
         fix_k1, fix_k2, fix_k3, fix_k4, fix_k5, fix_k6, intrinsic_guess, 
         save_keypoints, load_keypoints, debug):
    
    debug_folder = os.path.join(output_folder, "debug")
    undistorted_folder = os.path.join(output_folder, "undistorted")
    
    # Read config file
    assert os.path.exists(config_file)
    cfg = utils.json_read(config_file)

    # delete if exist
    utils.rmdir(debug_folder)
    utils.rmdir(undistorted_folder)

    utils.mkdir(undistorted_folder)
    if debug:
        utils.mkdir(debug_folder)

    print("-" * 50)
    print("Input parameters")
    print("-" * 50)
    print("folder_images:", folder_images)
    print("output_folder:", output_folder)
    print("description:", description)
    print("rational_model:", rational_model)
    print("alpha:", alpha)
    print("force_monotonicity:", force_monotonicity)
    print("monotonic_range:", monotonic_range)
    print("intrinsic_guess:", intrinsic_guess if len(intrinsic_guess) else False)
    print("fix_principal_point:", fix_principal_point)
    print("fix_aspect_ratio:", fix_aspect_ratio)
    print("zero_tangent_dist:", zero_tangent_dist)
    print("criteria_eps:", criteria_eps)
    print("threads:", threads)
    print("debug:", debug)
    print("-" * 50)

    current_datetime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
    if load_keypoints:
        keypoints = utils.json_read(os.path.join(output_folder, "keypoints.json"))
        objpoints = np.float32(keypoints['objpoints'])
        imgpoints = np.float32(keypoints['imgpoints'])
    else:
        # prepare object points, like (0,0,0), (30,0,0), (60,0,0) ....
        # each square is 30x30mm
        # NB. the intrinsic parameters, rvec and distCoeffs do not depend upon the landmarks/reference object size, tvec does instead!
        # TODO: find better more cohesive features: the current method used does not make it easier, the pattern is not classic and the landmarks are 
        #       visible on all cameras. The most obvious feture typeis pointns but then the images do not depict the entire board and cameras 1 and 5 
        #       look at it from behind. A line feature detector is what is used but then just to recinstruct a virtual volume CAD style, which is ok but
        #       there should not be all these loopholes for it
        # Fow we do it for srl5 (old images)
        
        # Features are box lines with a corner or an Aruco board or else
        if(False):
            objp = np.zeros((1,3), np.float32)
            objp[:,:2] = np.mgrid[0:1,0:1].T.reshape(-1,2)
            objp[:,:2] *= 1
        if(True):
            # Aruco board center with 8x6 squares of 3cm and 2.85 cm margins
            X = 0. + np.arange(0, 6) * 3.
            Y = 0. + np.arange(0, 8) * 3.
            xv, yv = np.meshgrid(X, Y)
            aruco = np.array([[x,y] for x,y in zip(xv.flatten(),yv.flatten()) ])
            # Correct order of aruco centers
            objp = np.vstack((aruco[::2,:],aruco[1::2,:]))
            objp = np.hstack((objp,np.zeros((48,1))))
        filename_images = utils.find_images(folder_images, "*")
        if len(filename_images) == 0:
            print("!!! Unable to detect images in this folder !!!")
            sys.exit(0)

        if threads>0:
            with multiprocessing.Pool(threads) as pool:
                res = pool.starmap(process_image, zip(filename_images, 
                                                      repeat(cfg),
                                                      repeat(debug),
                                                      repeat(debug_folder)))
        else:
            res = [process_image(f,cfg, debug, debug_folder) for f in filename_images]

        # res is coo and idx
        objpoints = [np.float32(objp.copy()[r[1]]) for r in res if r is not None] # 3d point in real world space
        imgpoints = [np.float32(r[0].copy()) for r in res if r is not None] # 2d points in image plane.
        
        # Find smallest set of common indices
        #idx_f = np.array(list(set(sorted(res[1][1])).intersection(sorted(res[0][1]), sorted(res[3][1]))))
        #idx_b = np.array(list(set(sorted(res[2][1])).intersection(sorted(res[4][1]))))
        # Two cases, front or behind
        # Front are 0,1,3 back are 2,4
        #objpoints_f = [ objpoints[i] for i in (0,1,3) ]
        #objpoints_b = [ objpoints[i] for i in (2,4) ]
        #imgpoints_f = [ imgpoints[i]  for i in (0,1,3) ]        
        #imgpoints_b = [ imgpoints[i] for i in (2,4) ]

        if save_keypoints:
            utils.json_write(os.path.join(output_folder, "keypoints.json"), {'objpoints':np.float32(objpoints).tolist(),
                                                                             'imgpoints':np.float32(imgpoints).tolist()})

    image = imageio.imread(filename_images[0])
    image_shape = image.shape[:2]
    
    # visualize the keypoints
    plt.figure()
    plt.plot(*np.vstack(imgpoints).squeeze().transpose(1,0), 'g.')
    plt.grid()
    plt.xlim(0, image_shape[1])
    plt.ylim(image_shape[0], 0)
    plt.savefig(os.path.join(output_folder, "detected_keypoints.jpg"), bbox_inches='tight')
    
    calib_flags = 0
    if rational_model:
        calib_flags += cv2.CALIB_RATIONAL_MODEL
    if fix_principal_point:
        calib_flags += cv2.CALIB_FIX_PRINCIPAL_POINT
    if fix_aspect_ratio:
        calib_flags += cv2.CALIB_FIX_ASPECT_RATIO
    if zero_tangent_dist:
        calib_flags += cv2.CALIB_ZERO_TANGENT_DIST
    if fix_k1:
        calib_flags += cv2.CALIB_FIX_K1
    if fix_k2:
        calib_flags += cv2.CALIB_FIX_K2
    if fix_k3:
        calib_flags += cv2.CALIB_FIX_K3
    if fix_k4:
        calib_flags += cv2.CALIB_FIX_K4
    if fix_k5:
        calib_flags += cv2.CALIB_FIX_K5
    if fix_k6:
        calib_flags += cv2.CALIB_FIX_K6        
        
    K_guess, dist_guess = None, None
    if len(intrinsic_guess):
        intrinsic_guess = utils.json_read(intrinsic_guess)
        K_guess = np.array(intrinsic_guess['K'])
        dist_guess = np.array(intrinsic_guess['dist'])
        calib_flags += cv2.CALIB_USE_INTRINSIC_GUESS
        
        print("K_guess:", K_guess)
        print("dist_guess:", dist_guess)

    print("working hard...")
    ret, mtx, distCoeffs, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, image_shape[::-1], None, None)
    
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, criteria_eps)
    
    # For front and back
    #iFixedPoint = 0
    #ret, mtx, distCoeffs, rvecs, tvecs, newObjPoints, \
    #stdDeviationsIntrinsics, stdDeviationsExtrinsics, \
    #stdDeviationsObjPoints, perViewErrors = cv2.calibrateCameraROExtended(objpoints, imgpoints, image_shape[::-1],
    #                                                                      iFixedPoint, K_guess, dist_guess,
    #                                                                      flags=calib_flags, criteria=criteria)
    
    def reprojection_error(mtx, distCoeffs, rvecs, tvecs):
        # print reprojection error
        reproj_error = 0
        for i in range(len(objpoints)):
            imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, distCoeffs)
            reproj_error += cv2.norm(imgpoints[i],imgpoints2.reshape(-1,2),cv2.NORM_L2)/len(imgpoints2)
        reproj_error /= len(objpoints) 
        return reproj_error
    
    reproj_error = reprojection_error(mtx, distCoeffs, rvecs, tvecs)
    print("RMS Reprojection Error: {}, Total Reprojection Error: {}".format(ret, reproj_error))
    
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, distCoeffs, image_shape[::-1], alpha, 
                                                      image_shape[::-1], centerPrincipalPoint=False)
    
    """
    grid_norm, is_monotonic = probe_monotonicity(mtx, distCoeffs, newcameramtx, image_shape, N=100, M=100)
    if not np.all(is_monotonic):
        print("-"*50)
        print(" The distortion function is not monotonous for alpha={:0.2f}!".format(alpha))
        print(" To fix this we suggest sampling more precise points on the corner of the image first.")
        print(" If this is not enough, use the option Rational Camera Model which more adpated to wider lenses.")
        print("-"*50)
    
    # visualise monotonicity
    plt.figure()
    plt.imshow(cv2.undistort(image, mtx, distCoeffs, None, newcameramtx))
    grid = grid_norm*newcameramtx[[0,1],[0,1]][None]+newcameramtx[[0,1],[2,2]][None]
    plt.plot(grid[is_monotonic, 0], grid[is_monotonic, 1], '.g', label='monotonic', markersize=1.5)
    plt.plot(grid[~is_monotonic, 0], grid[~is_monotonic, 1], '.r', label='not monotonic', markersize=1.5)
    plt.legend()
    plt.grid()
    plt.savefig(os.path.join(output_folder, "monotonicity.jpg"), bbox_inches='tight')
    """
    proj_undist_norm = np.vstack([cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], np.eye(3), None)[0].reshape(-1,2)
                                     for i in range(len(rvecs))])
    
    if force_monotonicity:
        is_monotonic = is_distortion_function_monotonic(distCoeffs, range=(0, monotonic_range, 1000))
        if is_monotonic:
            print("The distortion function is monotonic in the range (0,{:0.2f})".format(monotonic_range))
        else:
            print("The distortion function is not monotonic in the range (0,{:0.2f})".format(monotonic_range))

        if not is_monotonic:
            print("Trying to enforce monotonicity in the range (0,{:.2f})".format(monotonic_range))

            image_points = np.vstack(imgpoints)
            distCoeffs = enforce_monotonic_distortion(distCoeffs, mtx, image_points, proj_undist_norm, 
                                                      range_constraint=(0, monotonic_range, 1000))
            #TODO: refine mtx with the new distCoeffs in order to reduce reprojection error 

            newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, distCoeffs, image_shape[::-1], alpha, 
                                                              image_shape[::-1], centerPrincipalPoint=False)
            
            rvecs_new, tvecs_new = [],[]
            for objp,imgp in zip(objpoints, imgpoints):
                _, rvec, tvec = cv2.solvePnP(objp, imgp, mtx, distCoeffs) # is this the best?
                rvecs_new.append(rvec)
                tvecs_new.append(tvec)

            reproj_error = reprojection_error(mtx, distCoeffs, rvecs_new, tvecs_new)
            print("mono: RMS Reprojection Error: {}, Total Reprojection Error: {}".format(ret, reproj_error))

    d_json = dict({"date":current_datetime, "description":description,
                   "K":mtx.tolist(), "K_new":newcameramtx.tolist(), "dist":distCoeffs.ravel().tolist(),
                   "reproj_error":reproj_error, "image_shape":image_shape})

    utils.json_write(os.path.join(output_folder, "intrinsics.json"), d_json)

    # The code from this point on as the purpose of verifiying that the estimation went well.
    # images are undistorted using the compouted intrinsics
    
    # undistorting the images
    print("Saving undistorted images..")
    for i,filenames_image in enumerate(filename_images):

        img = imageio.imread(filenames_image)
        h, w = img.shape[:2]

        try:
            dst = cv2.undistort(img, mtx, distCoeffs, None, newcameramtx)
            # to project points on this undistorted image you need the following:
            # cv2.projectPoints(objpoints, rvec, tvec, newcameramtx, None)[0].reshape(-1,2)
            # or:
            # cv2.undistortPoints(imgpoints, mtx, distCoeffs, P=newcameramtx).reshape(-1,2)
            
            # draw principal point
            dst = cv2.circle(dst, (int(mtx[0, 2]), int(mtx[1, 2])), 6, (255, 0, 0), -1)

            imageio.imsave(os.path.join(undistorted_folder, os.path.basename(filenames_image)), dst)
        except:
            print("Something went wrong while undistorting the images. The distortion coefficients are probably not good. You need to take a new set of calibration images.")
            #sys.exit(0)

if __name__ == "__main__":
    
    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')
        
    parser = argparse.ArgumentParser()
    parser.add_argument("--folder_images", "-i", type=str, required=True)
    parser.add_argument("--output_folder", "-o", type=str, default='./output', required=False)
    parser.add_argument("--description", "-d", type=str, default="", required=False,
                        help="Optional description to add to the output file.")
    
    parser.add_argument("--config_file", "-cfg", type=str, default=0.95, required=True,
                        help="Config file explaining the pattern and size of reference marker as well as covered pattern region.")
    parser.add_argument("--alpha", "-a", type=float, default=0.95, required=False,
                        help="Parameter controlling the ammount of out-of-image pixels (\"black regions\") retained in the undistorted image.")
    parser.add_argument("--threads", "-t", type=int, default=4, required=False)
    
    # Added from https://github.com/cvlab-epfl/multiview_calib
    # Looks to be an important part for error checking, see https://inria.hal.science/inria-00384203/document
    parser.add_argument("--force_monotonicity", "-fm", type=str2bool, default=False, required=False,
                        help="Force monotonicity in the range defined by monotonic_range. To be used only in extreme cases.")
    parser.add_argument("--monotonic_range", "-mr", type=float, default=-1, required=False,
                        help="Value defining the range for the distortion must be monotonic. Typical value to try 1.3. Be careful: increasing this value may negatively perturb the distortion function.")
    parser.add_argument("--rational_model", "-rm", action="store_true", required=False,
                        help="Use a camera model that is better suited for wider lenses.")
    parser.add_argument("--fix_principal_point", "-fpp", action="store_true", required=False,
                        help="Fix the principal point either at the center of the image or as specified by intrisic guess.")
    parser.add_argument("--fix_aspect_ratio", "-far", action="store_true", required=False) 
    parser.add_argument("--zero_tangent_dist", "-ztg", action="store_true", required=False) 
    
    parser.add_argument("--criteria_eps", "-eps", type=float, default=1e-5, required=False,
                        help="Precision criteria. A larger value can prevent overfitting and artifacts on the borders.")
    
    # add flag in opencv calibration tool for especially polinomila coefficient of radial distortion
    parser.add_argument("--fix_k1", "-k1", action="store_true", required=False)
    parser.add_argument("--fix_k2", "-k2", action="store_true", required=False)
    parser.add_argument("--fix_k3", "-k3", action="store_true", required=False)
    parser.add_argument("--fix_k4", "-k4", action="store_true", required=False)
    parser.add_argument("--fix_k5", "-k5", action="store_true", required=False)
    parser.add_argument("--fix_k6", "-k6", action="store_true", required=False)
    
    # Help!
    parser.add_argument("--intrinsic_guess", "-ig", type=str, required=False, default="",
                        help="JSON file containing a initial guesses for the intrinsic matrix and distortion parameters.")
    # 
    parser.add_argument("--save_keypoints", action="store_true", required=False)
    parser.add_argument("--load_keypoints", action="store_true", required=False)
    parser.add_argument("--debug", action="store_true", required=False)
    args = parser.parse_args()
    
    main(**vars(args))  
    
 