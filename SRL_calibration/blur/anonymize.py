# Copyright 2024 adakri
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import cv2
import argparse
import os, sys
sys.path.append('./')
import SRL_calibration.calibration.utils as utils
from itertools import repeat
import multiprocessing
import imageio

# Check for OpenCV version for function (could pose issue in Windows) 
(cv2_major, cv2_minor, _) = cv2.__version__.split(".")
if int(cv2_major)<4:
    raise ImportError("Opencv version 4+ required!")



def blur_face(filename_image,debug, debug_folder,):
    print("Processing image {} ...".format(filename_image))

    # Read image
    colour = imageio.imread(filename_image)
    if(colour.shape[0] < colour.shape[1]):
        img = cv2.rotate(imageio.imread(filename_image), cv2.ROTATE_90_COUNTERCLOCKWISE)
        
    # Load the pre-trained face detection model
    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
    # Convert the img to grayscale for face detection
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Perform face detection
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=1, minSize=(30, 30))
    # Iterate over the detected faces and blur them
    for (x, y, w, h) in faces:
        # Extract the face ROI (Region of Interest)
        face_roi = img[y:y+h, x:x+w]
        # Apply Gaussian blur to the face ROI
        face_roi = cv2.GaussianBlur(face_roi, (99, 99), 30)
        # Replace the face in the original img with the blurred face
        img[y:y+h, x:x+w] = face_roi
    # Display the img with blurred faces
    cv2.imshow('Face Blurring', img)

    # Exit the loop if the 'q' key is pressed
    cv2.waitKey(0)
        
    cv2.imwrite(os.path.join(debug_folder, os.path.basename(filename_image)), img)


def main(folder_images, output_folder,debug):
    debug_folder = os.path.join(output_folder, "debug")
    blur_folder = debug_folder = os.path.join(output_folder, "blurred")
    utils.mkdir(blur_folder)
    if debug:
        utils.mkdir(debug_folder)
        
    print("-" * 50)
    print("Input parameters")
    print("-" * 50)
    print("folder_images:", folder_images)
    print("output_folder:", output_folder)
    print("debug:", debug)
    print("-" * 50)    
    
    filename_images = utils.find_images(folder_images, "*")
    if len(filename_images) == 0:
        print("!!! Unable to detect images in this folder !!!")
        sys.exit(0)

    res = [blur_face(f, debug, debug_folder) for f in filename_images] 
        
    
    return      



if __name__ == "__main__":
    
    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')
        
    parser = argparse.ArgumentParser()
    parser.add_argument("--folder_images", "-i", type=str, required=True)
    parser.add_argument("--output_folder", "-o", type=str, default='./output', required=False)
    parser.add_argument("--debug", action="store_true", required=False)
    args = parser.parse_args()

    main(**vars(args))  