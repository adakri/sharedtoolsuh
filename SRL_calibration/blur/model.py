# Copyright 2024 adakri
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import torch


def render_images_from_scan(scan_mesh, pose_type, output_folder, device, dtype, save_img=True, mesh_fn=None):
    from os.path import join
    import cv2

    from mmdet.apis import init_detector
    from mmpose.apis import init_pose_model

    use_hands = True
    imsize = 1000
    pp = imsize / 2.
    focal_len = 1500.
    dist_to_cam = 4.

    pose_det_model = None
    pose_kp_model = None
    # pose_type = 'mediapipe' #'yolox+hrnet_w48_dark_plus' #

    print('Using {} to detect keypoints for AFS!!'.format(pose_type))

    if pose_type == 'yolox+hrnet_w48_dark_plus':
        det_cfg_path = join('amalthea', 'core', 'utils', 'mm_files', 'mmdet', 'configs', 'yolox',
                            'yolox_x_8x8_300e_coco.py')
        det_ckpt_path = join('deps', 'yolox_x_8x8_300e_coco_20211126_140254-1ef88d67.pth')

        pose_det_model = init_detector(det_cfg_path, det_ckpt_path, device=device)

        pose_cfg_path = join('amalthea', 'core', 'utils', 'mm_files', 'mmpose', 'configs', 'wholebody',
                             '2d_kpt_sview_rgb_img', 'topdown_heatmap', 'coco-wholebody',
                             'hrnet_w48_coco_wholebody_384x288_dark_plus.py')
        pose_ckpt_path = join('deps', 'hrnet_w48_coco_wholebody_384x288_dark-f5726563_20200918.pth')

        pose_kp_model = init_pose_model(pose_cfg_path, pose_ckpt_path, device=device)
    elif pose_type == 'yolox+tcformer':
        from amalthea.core.utils.mm_files.mmpose.tools import tcformer  # to register the model in the registry
        det_cfg_path = join('amalthea', 'core', 'utils', 'mm_files', 'mmdet', 'configs', 'yolox',
                            'yolox_x_8x8_300e_coco.py')
        det_ckpt_path = join('deps', 'yolox_x_8x8_300e_coco_20211126_140254-1ef88d67.pth')

        pose_det_model = init_detector(det_cfg_path, det_ckpt_path, device=device)

        pose_cfg_path = join('amalthea', 'core', 'utils', 'mm_files', 'mmpose', 'configs', 'wholebody',
                             '2d_kpt_sview_rgb_img', 'topdown_heatmap', 'coco-wholebody',
                             'tcformer_large_mta_coco_wholebody_384x288.py')
        pose_ckpt_path = join('deps', 'tcformer_large_coco-wholebody_384x288-b3b884c8_20220627.pth')

        pose_kp_model = init_pose_model(pose_cfg_path, pose_ckpt_path, device=device)

    verts_rgb = torch.ones_like(torch.tensor(scan_mesh.v), dtype=dtype, device=device).unsqueeze(
        dim=0)
    # verts_rgb *= torch.tensor([.85, .85, 1.], device=device, dtype=dtype)
    textures = TexturesVertex(verts_features=verts_rgb)
    
    bg_color = (1., 1., 1.) #(0., 0., 0.)

    scan_mesh_py3d = Meshes(verts=torch.tensor(scan_mesh.v, device=device, dtype=dtype).unsqueeze(dim=0),
                            faces=torch.tensor(scan_mesh.f.astype(np.int32), device=device, dtype=dtype).unsqueeze(
                                dim=0),
                            textures=textures) 
    # Add colour if inria
    if(mesh_fn is not None and mesh_fn[-3:] == 'ply'):
            from pytorch3d.io import IO
            mesh = IO().load_mesh(mesh_fn, device=device)
            # Scale 
            scan_mesh_py3d = Meshes(verts=torch.tensor(scan_mesh.v, device=device, dtype=dtype).unsqueeze(dim=0),
                            faces=torch.tensor(scan_mesh.f.astype(np.int32), device=device, dtype=dtype).unsqueeze(
                                dim=0),
                            textures=mesh.textures) 
    
    scan_mean = scan_mesh.v.mean(axis=0)

    cam_look_at_pt = torch.tensor(scan_mean, device=device, dtype=dtype).unsqueeze(dim=0)

    output_dict = {}

    for i, view in enumerate(['front', 'back', 'left_up', 'right_up']): #, 'left', 'right']):

        ignore_kps_mask = np.zeros((67), dtype=bool)

        if view == 'front':
            cam_pos = torch.tensor([scan_mean + np.array([0., 0., dist_to_cam])], device=device, dtype=dtype)
            light_pos = [0., 0., dist_to_cam]

            # ignore heel keypoints for front view
            ignore_kps_mask[[21, 24]] = True

        elif view == 'back':
            cam_pos = torch.tensor([scan_mean + np.array([0., 0., -dist_to_cam])], device=device, dtype=dtype)
            light_pos = [0., 0., -dist_to_cam]

            # ignore face and toe keypoints for back view
            ignore_kps_mask[[0, 15, 16, 17, 18, 19, 20, 22, 23]] = True
        elif view == 'left':
            cam_pos = torch.tensor([scan_mean + np.array([-dist_to_cam, 0., 0.])], device=device, dtype=dtype)
            light_pos = [-dist_to_cam, 0., 0.]

            # ignore heel keypoints for front view
            ignore_kps_mask[[5, 12, 16, 18]] = True

        elif view == 'right':
            cam_pos = torch.tensor([scan_mean + np.array([dist_to_cam, 0., 0.])], device=device, dtype=dtype)
            light_pos = [dist_to_cam, 0., 0.]

            # ignore face and toe keypoints for back view
            ignore_kps_mask[[2, 9, 15, 17]] = True

        elif view == 'left_up':
            cam_pos = torch.tensor([scan_mean + np.array([-dist_to_cam / 2., dist_to_cam / 2., 0.])], device=device, dtype=dtype)
            light_pos = [-dist_to_cam, dist_to_cam / 2., 0.]

            # ignore heel keypoints for front view
            ignore_kps_mask[[5, 12, 16, 18]] = True

        elif view == 'right_up':
            cam_pos = torch.tensor([scan_mean + np.array([dist_to_cam / 2., dist_to_cam / 2., 0.])], device=device, dtype=dtype)
            light_pos = [dist_to_cam / 2., dist_to_cam / 2., 0.]

            # ignore face and toe keypoints for back view
            ignore_kps_mask[[2, 9, 15, 17]] = True

        cam_R_py3d, cam_t_py3d = look_at_view_transform(eye=cam_pos, at=cam_look_at_pt, device=device)

        renderer = get_renderer(fx=focal_len,
                                fy=focal_len,
                                cx=pp,
                                cy=pp,
                                device=device,
                                width=imsize,
                                height=imsize,
                                type='mesh',
                                shrink_factor=1.0,
                                light_pos=light_pos, # not possible to set multiple light sources in py3d!
                                bg_color=bg_color)


        # render_im = renderer(meshes_world=scan_mesh_py3d.clone(), R=cam_R_py3d, T=cam_t_py3d)

        # which pixel shows which mesh face
        fragments = renderer.rasterizer(meshes_world=scan_mesh_py3d.clone(), R=cam_R_py3d, T=cam_t_py3d)
        render_im = renderer.shader(fragments, scan_mesh_py3d.clone(), R=cam_R_py3d, T=cam_t_py3d)

        img = (render_im[0, ..., :3].detach().cpu().numpy().squeeze() * 255.).astype(np.uint8)

        if save_img:
            cv2.imwrite(join(output_folder, 'scan_render_{}.jpg'.format(view)), img)

        kp_dict = run_pose_prediction(image=img,
                                      pose_det_model=pose_det_model,
                                      pose_kp_model=pose_kp_model,
                                      pose_type=pose_type,
                                      save_fn_kp_img=join(output_folder,'scan_render_{}_mp_keypoints.jpg'.format(view)))

        keypoint_data = read_keypoints(keypoint_fn=None,
                                       kp_dict=kp_dict,
                                       use_hands=use_hands,
                                       use_face=False,
                                       use_face_contour=False)

        if keypoint_data is not None:
            kps_2d = torch.tensor([keypoint_data.keypoints[0][:, :2]], device=device, dtype=dtype)
            kps_2d_conf = torch.tensor([keypoint_data.keypoints[0][:, 2]], device=device, dtype=dtype)

            pix_to_face = fragments.pix_to_face.detach().cpu().numpy().squeeze()
            kp_vals = keypoint_data.keypoints[0][:, :2]  # round or just int?
            face_inds_at_kps = np.ones((kp_vals.shape[0]), dtype=int) * -1

            for kp_ind, cur_kp in enumerate(kp_vals):
                if 0 <= cur_kp[1] < img.shape[0] and 0 <= cur_kp[0] < img.shape[1]:
                    face_inds_at_kps[kp_ind] = pix_to_face[int(cur_kp[1]), int(cur_kp[0])]

            kps_surface = torch.tensor([scan_mesh.v[scan_mesh.f[face_inds_at_kps]].mean(axis=1)],
                                       device=device, dtype=dtype)
            kps_surface_conf = torch.tensor([keypoint_data.keypoints[0][:, 2]], device=device, dtype=dtype)

            # set kps without correspondence to 0
            kps_surface[:, face_inds_at_kps == -1] = 0.
            kps_surface_conf[:, face_inds_at_kps == -1] = 0.

            if True in ignore_kps_mask:
                # set kps to ignore to 0
                kps_surface[:, ignore_kps_mask] = 0.
                kps_surface_conf[:, ignore_kps_mask] = 0.

            output_dict[view] = {'img': img,
                                 'kps_2d': kps_2d,
                                 'kps_2d_conf': kps_2d_conf,
                                 'kps_3d': kps_surface,
                                 'kps_3d_conf': kps_surface_conf,
                                 'kp_cam_r_py3d': cam_R_py3d,
                                 'kp_cam_t_py3d': cam_t_py3d,
                                 'rasterizer': renderer.rasterizer}

    return output_dict