# Copyright 2024 adakri
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os, sys
from PIL import Image
import xmltodict
from data import * 
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import argparse

# Project points using opencv data context from agi calibration tool
def project_points(pts, intr, extr):
    assert pts.shape[-1] == 3
    return cv.projectPoints(objectPoints=pts, 
                            rvec=np.fromstring(extr['opencv_storage']['R']['data'], dtype=float, sep=' ').reshape(3,3), 
                            tvec=np.fromstring(extr['opencv_storage']['T']['data'], dtype=float, sep=' '), 
                            cameraMatrix=np.fromstring(intr['opencv_storage']['M']['data'], dtype=float, sep=' ').reshape(3,3), 
                            distCoeffs=np.fromstring(intr['opencv_storage']['D']['data'], dtype=float, sep=' '),)
# Test function for agi calibration validation (with opencv model)
def dome_func():
    # Open images
    images = [Image.open(f) for f in sorted(dome_data['images'])]
    
    # Points to be projected
    pts = np.array([
        [0.,0.,0.],
        [0.,0.,-1.],
        [0.,-1.,0.], 
        [0.,-1.,-1.],
        [1.,0.,0.],
        [1.,0.,-1.],
        [1.,-1.,0.], 
        [1.,-1.,-1.],]) / 4.
    
    params_opencv = {}
    number_of_cameras = CAM_NUM
    # Generate camera parameters for opencv calibration
    for idx in range(number_of_cameras):
        # Switch to opencv XML
        extr = xmltodict.parse(open(dome_data["opencv_calibs"][idx]["extrinsics"]).read())
        
        intr = xmltodict.parse(open(dome_data["opencv_calibs"][idx]["intrinsics"]).read())
        # Project points on image and visualize
        projected_pts, _ = project_points(pts, intr, extr)
        projected_pts = projected_pts.reshape(-1,2)
        
        fig, ax = plt.subplots(figsize=(10,10))
        im = plt.imshow(images[idx])
        for num, i in enumerate(projected_pts):
            if(num<4):
                circle = plt.Circle((i[0], i[1]), color='green')
                ax.add_patch(circle)
            elif(num>=4):
                circle = plt.Circle((i[0], i[1]), color='yellow')
                ax.add_patch(circle)
        # Connectivity
        connectivity = [(0,1), (0,4), (0,2), (7,6), 
                        (7,5), (7,3), (1,5), (5,4), 
                        (6,2), (6,4), (2,3), (1,3)]
        for pair in connectivity:
            plt.plot([projected_pts[pair[0]][0], projected_pts[pair[1]][0]], 
                     [projected_pts[pair[0]][1], projected_pts[pair[1]][1]], 
                     'o-')
        
        plt.show()
        print()

    
    return

# Calibrate and validate with random images (special case for uh) 
def uh_func():
    parser = argparse.ArgumentParser(
                    description='Calibrate and validate using OpenCV',
                    epilog='Text at the bottom of help')
    parser.add_argument('-im', '--images', description='Location of images')
    
    # Basic
    """
    * Detect tailored features
    * Select view consistent features
    * basic multiview calibration
    """
    # Advanced
    """
    * Bundle adjustment
    * monotonic distortion
    """
    
      
    return
    
    
if __name__ == "__main__":
    dome_func()