import torch
import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import os, sys
from collections import defaultdict
from pytorch3d.io import load_objs_as_meshes
from tqdm import tqdm
#from ipywidgets import widgets
import plotly.express as px


# Use tinyobjloader instead of trimesh
#import tinyobjloader 
from dataclasses import dataclass


@dataclass
class obj:
    """Struct for Objs 
    """
    verts: torch.tensor
    faces: torch.tensor
    
"""
Data structure that describes the layout of the folder to be anaylyzed. 
"""
data_strcuture = {
    'original_obj': 'mesh_0_scaled.ply',
    'posed_freev': 'img_0_freev.obj',
    'Tposed_freev': 'img_0_reposed_t.obj',
    'posed_smpl': 'img_0.obj',
    'Tposed_smpl': 'tpose_img_0.obj'
}
    

class miniApp():
    def __init__(self,
                 data_folder: str,
                 title: str="Testing",
                 use_py3d: bool=True,
                 device=None,
                **kwargs,):
        self._title=title
        self._data_folder = data_folder
        self._fn = []
        # Additinal attribs
        self._fig = None
        # Buffer attribs
        self._meshes = defaultdict(list)
        self._errors = defaultdict(list)
        # Flags
        self._use_py3d = use_py3d
        if(device is None):
            self._device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        else:
            self._device = device

    # https://github.com/DavidBoja/SMPL-Anthropometry/tree/master
    @staticmethod
    def load_mesh(fileName: str,
                  device='cpu',
                  verbose=True)-> obj:
        """Load PLY or OBJ meshes

        Args:
            fileName (str): Path to file to open
            device (str, optional): Device of computation. Defaults to 'cpu'.

        Returns:
            obj: _description_
        """
        # Transform
        from pytorch3d.transforms import RotateAxisAngle
        t1 = RotateAxisAngle(axis="X", angle=270.)
        t2 = RotateAxisAngle(axis="Y", angle=-90.)
        t = t1.compose(t2)
        import warnings
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            from pytorch3d.io import IO
            if(fileName[-3:] == 'obj'):
                try:
                    # Try texture reading with file
                    mesh = load_objs_as_meshes(
                        [fileName], device=device
                    )  # TODO: add warning here
                    vertices = t.transform_points(mesh.verts_packed()).to(device)
                    faces = mesh.faces_packed().to(device)
                except Exception as e:
                    print("Failed to read file. Reason: %s" % (e))
                    sys.exit(1)
            elif(fileName[-3:] == 'ply'):
                from pytorch3d.io import IO
                mesh = IO().load_mesh(fileName, device=device)
                vertices = t.transform_points(mesh.verts_packed().to(device))
                faces = mesh.faces_packed().to(device)
                try: 
                    assert vertices.shape[0] != 0
                except Exception:
                    pcd = IO().load_pointcloud(fileName, deviec=device)
                    vertices = t.transform_points(pcd.points_packed())
                    faces = None
                
            else:
                print(f"Format {fileName[-3:]} is unkown")
        if(verbose):
            print("Successfly loaded mesh!")
        return obj(verts=vertices, faces=faces)

    # https://community.plotly.com/t/display-a-custom-colorbar-with-mesh3d-and-vertexcolor/56202
    def add_to_scene(self,
                     verts : torch.tensor, 
                     faces : torch.tensor,
                     intensity: torch.tensor,
                     row: int=1,
                     col: int=1) -> None:
        """Add mesh to scene

        Args:
            verts (torch.tensor): (N,3)
            faces (torch.tensor): (F,3)
            intensity (torch.tensor): Field to visualize on mesh (N,)
            row (int, optional): If subplots row index. Defaults to 1.
            col (int, optional): If subplots column index. Defaults to 1.
        """
        mesh_plot = go.Mesh3d(
                            x=verts[:,0],
                            y=verts[:,1],
                            z=verts[:,2],
                            intensity=torch.abs(intensity),
                            hovertemplate ='<i>Index</i>: %{text}',
                            text = [i for i in range(verts.shape[0])],
                            showscale=True,
                            colorscale="jet",
                            colorbar_x = 0.45 * (col-1),
                            # i, j and k give the vertices of triangles
                            i=faces[:,0],
                            j=faces[:,1],
                            k=faces[:,2],
                            opacity=0.6,
                            name='body',
                            )
        self._fig.add_trace(mesh_plot, row=row, col=col)  
        
    def parse_dataset(self,
                      verbose=True) -> None:
        """Parse folder where files reside.
        """
        self._len = len(os.listdir(self._data_folder))
        for fn in tqdm(os.listdir(self._data_folder)):
            if(fn=='ps_shape' or '.' in fn):
                self._len -= 1
                print(f"Skipped file: {fn}")
                continue
            
            fn = os.path.join(self._data_folder, fn)
            if(not os.path.exists(fn)):
                self._len -= 1
                print(f"Skipped file: {fn}")
            else:
                self._fn.append(fn)
                # Get files
                self._meshes['original_obj'].append(miniApp.load_mesh(os.path.join(fn, data_strcuture['original_obj']), verbose=verbose))
                self._meshes['posed_freev'].append(miniApp.load_mesh(os.path.join(fn, data_strcuture['posed_freev']), verbose=verbose))
                self._meshes['posed_smpl'].append(miniApp.load_mesh(os.path.join(fn, data_strcuture['posed_smpl']), verbose=verbose))
            
    def runAppCompute(self)-> None:
        """Compute error metrics
        """
        # Compute errors
        #=============================
        # Compute average shape metrics
        verts_tpose = [mesh.verts for mesh in self._meshes['Tposed_freev']]
        mean_tpose = torch.mean(torch.stack(verts_tpose, dim=0), dim=0)
        
        # Mesh to scan error: (freev reg - scan)
        for i in tqdm(range(len(self._meshes['original_obj']))):
            if(self._use_py3d):
                from pytorch3d.loss.point_mesh_distance import point_face_distance
                from pytorch3d.structures import Meshes, Pointclouds
                
                #=============================
                # Compute mesh to scan distance
                pcls = Pointclouds(points=self._meshes['original_obj'][i].verts.clone().detach().to(device=self._device, dtype=torch.float32).unsqueeze(dim=0))
                meshes = Meshes(verts=self._meshes['posed_freev'][i].verts.clone().detach().to(device=self._device, dtype=torch.float32).unsqueeze(dim=0),
                                faces=self._meshes['posed_freev'][i].faces.clone().detach().to(device=self._device, dtype=int).unsqueeze(dim=0))
                pcls_model = Pointclouds(points=self._meshes['posed_freev'][i].verts.clone().detach().to(device=self._device, dtype=torch.float32).unsqueeze(dim=0))
                # packed representation for pointclouds
                points = pcls.points_packed() # (P, 3)
                points_first_idx = pcls.cloud_to_packed_first_idx()
                max_points = pcls.num_points_per_cloud().max().item()

                # packed representation for faces
                verts_packed = meshes.verts_packed()
                faces_packed = meshes.faces_packed()
                tris = verts_packed[faces_packed]  # (T, 3, 3)
                tris_first_idx = meshes.mesh_to_faces_packed_first_idx()
                max_tris = meshes.num_faces_per_mesh().max().item()

                # point to face distance: shape (P,)
                point_to_face = point_face_distance(
                    points, points_first_idx, tris, tris_first_idx, max_points
                )

                diff = point_to_face * 1000. # mesh to scan
                self._errors['s2m_l1'].append(torch.linalg.norm(diff, ord = 1).item()) 
                self._errors['s2m_l2'].append(torch.linalg.norm(diff, ord = 2).item()) 
                self._errors['s2m_perVertex'].append(diff)
                diff = torch.linalg.norm(self._meshes['Tposed_freev'][i].verts * 1000. - mean_tpose* 1000., ord=2, dim=1)
                self._errors['distance_to_meanPose'].append(diff)
                self._errors['distance_to_meanPose_l2'].append(torch.linalg.norm(diff, ord = 2).item())
       
    def boxPlot(self,
                key: str = 's2m_perVertex'):
        """Generate box plot through a hack. TODO: clean and efficient box plot generation

        Args:
            key (str, optional): Which field to generate the boxplot from. Defaults to 's2m_perVertex'.
        """
        for i in range(self._len):
            self._fig.add_trace(go.Box(y=self._errors[key][i].cpu().numpy(),
                                       name=str(i),
                                       boxpoints=False, # no data points
                                       visible=False,
                                       )
                                )
        
        
    def VersatilePlot(self,)-> None:
        """Generate plot
        """
        self._fig = go.Figure()
        # TODO: Add widgets and in browser rendering
        # Bug with renderer backend in webGL ?
        self._fig.update_layout(title_text="Distance metrics")
        self._fig.update_layout(showlegend=False)
        # Edit axis labels
        self._fig['layout']['xaxis']['title']='Frames'
        self._fig['layout']['yaxis']['title']='Scan to mesh distance in mm'
        # Update figure
        # Draw scatter plot
        barPlot = go.Bar(x=list(range(self._len)), y=self._errors['s2m_l2'])
        self._fig.add_trace(
            barPlot,
        )
        barPlot = go.Bar(x=list(range(self._len)), y=self._errors['s2m_l1'], visible=False)
        self._fig.add_trace(
            barPlot,
        )
        # Draw Box plot
        self.boxPlot()
        activation_mask_l2 = [True, False] +  [False for i in range(self._len)]
        activation_mask_l1 = [True, False] +  [False for i in range(self._len)]
        activation_mask_bp = [False, False] +  [True for i in range(self._len)]
        
        
        # Add dropdown
        self._fig.update_layout(
            updatemenus=[
                dict(
                    active=0,
                    buttons=list([
                        dict(label="s2m_l2",
                            method="update",
                            args=[{"visible": activation_mask_l2},
                                {"title": "Scan to mesh loss - L2 norm",
                                    "annotations": []}]),
                        dict(label="s2m_l2",
                            method="update",
                            args=[{"visible": activation_mask_l1},
                                {"title": "Scan to mesh loss - L2 norm",
                                    "annotations": []}]),
                        dict(label="Box plot",
                            method="update",
                            args=[{"visible": activation_mask_bp},
                                {"title": "Median and Max/Min values",
                                    "annotations": []}]),
                    ]),
                ),
            ])
        #self._fig.show()
        # Meta figure containing widgets
        self._meta_fig = go.FigureWidget(self._fig)
        
        # Box plot
        barplot = self._meta_fig.data[0]
        self._meta_fig.layout.hovermode = 'closest'
        # Render
        # Click callback funtion
        from ipywidgets import Output, VBox
        out = Output()
        @out.capture(clear_output=True)
        def update_point(trace, points, selector):
            print("update")
            self.renderSMPL(points.point_inds[0])
        
        # Add callbacks to plot 
        barplot.on_click(update_point)
        
        # Render (Works onlt in ipyNB, timeout) TODO
        box = VBox([self._meta_fig, out])
        from IPython.display import display
        display(box)
        
    def renderSMPL(self,
                   index: int=0)-> None:
        """Render 3D interactive Mesh of SMPL

        Args:
            index (int, optional): _description_. Defaults to 0.
        """
        self._fig = make_subplots(rows=1, cols=2,
                                  specs=[[{'is_3d': True}, {'is_3d': True}]],
                                  subplot_titles=[ f'Posed free vertices model: frame N°{index} s2m error per vertex', 
                                                   f'T Posed free vertices model: frame N°{index}: per Vertex distance to the mean shape'],
                                  )
        # draw axes in proportion to the proportion of their ranges
        self._fig.update_layout(scene_aspectmode='data')
        # Camera Init
        camera = dict(
            up=dict(x=0, y=1., z=0.),
            eye=dict(x=0., y=0., z=2.5)
        )
        self._fig.update_scenes(
                  camera=camera,
                  aspectmode='data',
                  row=1, col=1)
        self._fig.update_scenes(
                  camera=camera,
                  aspectmode='data',
                  row=1, col=2)
        
        # Draw
        self.add_to_scene(self._meshes['posed_freev'][index].verts, 
                          self._meshes['posed_freev'][index].faces, 
                          intensity=self._errors['s2m_perVertex'][index].cpu())
        self.add_to_scene(self._meshes['Tposed_freev'][index].verts, 
                          self._meshes['posed_freev'][index].faces, 
                          intensity=self._errors['distance_to_meanPose'][index].cpu(),
                          row=1, col=2)
        
        self._fig.update_layout(title_text=f"Model Metrics: {self._fn[index]}")
        self._fig.show()
        
        
    def runApp(self)-> None:  
        """Main function
        """
        print("Parsing dataset")
        self.parse_dataset()
        print("Computing")
        self.runAppCompute()
        print("Rendering Box Plot")
        self.VersatilePlot()
        
        
    