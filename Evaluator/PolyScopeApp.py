from utils import obj, data_strcuture, miniApp
import polyscope as ps
from tqdm import tqdm
import os, sys
import numpy as np
import polyscope.imgui as psim
import matplotlib.pyplot as plt



list_of_meshes_to_load = [
    'original_obj', 'posed_freev', 'posed_smpl'
]

# Non local default callback options
frame_number = 0
onBoot = True
comboBox = 0
checkbox = {}
checkbox['original_obj'] = 'Input PLY file'
checkbox['posed_freev'] = 'Free vertices fitting'
checkbox['posed_smpl'] = 'Posed smpl fitting'
checkbox['all'] = True


class psAPP(miniApp):
    def __init__(self,
                 data_folder: str,
                 title: str="Testing",
                 use_py3d: bool=True,
                 simple: bool=False,
                 device=None,
                **kwargs,):
        ps.init()
        self._data_root = data_folder
        self._noCompute = simple
        self._participants = []
        if(not os.path.exists(self._data_root)):
            ps.error(f"The folder {self._data_root} does not exist")
            sys.exit(1)
        
        if(len(os.listdir(self._data_root)) < 1 ):
            ps.error(f"The folder {self._data_root} is empty")
            sys.exit(1)
            
        for name in os.listdir(self._data_root):
            if('03KEI' in name  and name[-3:]!='png' or True):
                self._participants.append(name)
            else:
                ps.warning(f"participant {name} is not recongnized")
                
        # Order participants list
        self._participants = sorted(self._participants)
    
        super(psAPP, self).__init__(data_folder=data_folder,
                         title=title,
                         use_py3d=use_py3d,
                         device=device,
                         **kwargs)
        

        
    
    def jit_parse_dataset(self):
        self._data_folder = os.path.join(self._data_root, self._participants[comboBox])
        if(not os.path.exists(self._data_folder)):
            ps.warning(f"Participant {comboBox} does not exist, choose another one")
            
        self._len = len(os.listdir(self._data_folder))
        for fn in tqdm(os.listdir(self._data_folder)):
            if(fn=='ps_shape'):
                self._len -= 1
                print(f"Skipped file: {fn}")
                continue
            
            fn = os.path.join(self._data_folder, fn)
            if(not os.path.exists(fn)):
                self._len -= 1
                print(f"Skipped file: {fn}")
            else:
                self._fn.append(fn)        

    # Callbacks
    def register_frame(self,
                       fn: str)->None:
        # To numpy
        # We need to load the meshes to memory to compute but still dont want them hangung around
        for j in list_of_meshes_to_load:
            self._meshes[j].append( super().load_mesh(os.path.join(fn, data_strcuture[j])))
            ps_mesh = ps.register_surface_mesh(
                j+"_"+fn, 
                self._meshes[j].verts.cpu().numpy(),
                self._meshes[j].faces.cpu().numpy(),
            )
    def register_frames(self,):
        for frame in self._fn:
            self.register_frame(frame)
            
            
    def callback_simple(self,)->None:
                
        def general_callback():
            global frame_number, checkbox, onBoot
                        
            psim.TextUnformatted("Frame Control")
            psim.Separator()
            
            # Offset
            offset = np.array([1.0,0.0,0.0])
            
            if(psim.Button("Refresh") or onBoot):
                # This code is executed when the button is pressed
                print("Parsing dataset")
                #self.jit_parse_dataset()
                
                ####################################
                # Reparse the dataset and run app compute
                # See how much mem is needed to load all meshes
                # If not good redo like b4: recall register frame each time
                print(f"Parsing Dataset for participant {self._participants[comboBox]}")
                # Reset meshes data structure
                from collections import defaultdict
                self._meshes = defaultdict(list)
                self.parse_dataset(verbose=False)
                onBoot = True

            # By default all meshes are shown
                    
            # Slider to choose frame
            participant_code = self._fn[frame_number].split("/")[-1]
            psim.TextUnformatted(f"Participant {participant_code}")
            psim.Separator()
            
            if(onBoot):
                f = open('./log.txt', 'w')
            
            
            if(psim.Button("Note")):
                print(f"code {participant_code} selected")
                f = open('./log.txt', 'a+')
                f.write(participant_code+'\n')
                f.close()
                onBoot = True
            
            changed, frame_number = psim.SliderInt(label=f"Frame number", 
                                                   v=frame_number, 
                                                   v_min=0, 
                                                   v_max=self._len-1)
            

            # Add interaction on left arrow press
            #import imgui
            #LEFT_ARROW = imgui.GetKeyIndex(imgui.ImGuiKey_LeftArrow)
            changed_button_right = False
            changed_button_left = False
            if(psim.Button("Left")):
                frame_number = frame_number-1 if frame_number >=1 else 0
                changed_button_left = True
            if(psim.Button("Right")):
                frame_number = frame_number+1 if frame_number < len(self._fn)-1 else 0
                changed_button_right = True
            
            if(changed or onBoot or changed_button_left or changed_button_right):
                print(frame_number)
                # Call register frame if the meme is not suff
                for i,meshType in enumerate(list_of_meshes_to_load):
                    mesh = self._meshes[meshType][frame_number]
                    if(mesh.faces is not None):
                        ps_mesh = ps.register_surface_mesh(checkbox[meshType], 
                                                           vertices=i*offset + mesh.verts.cpu().numpy() - [0.,np.min(mesh.verts.cpu().numpy()[:,1]), 0.], 
                                                           faces=mesh.faces.cpu().numpy())
                    else:
                        # Point cloud
                        ps_mesh = ps.register_point_cloud(checkbox[meshType], 
                                                           points=i*offset + mesh.verts.cpu().numpy() - [0.,np.min(mesh.verts.cpu().numpy()[:,1]), 0.], 
                                                           point_render_mode='sphere',
                                                           radius = 0.001)
                
                
                # Add superposition of freev and pcd
                mesh = self._meshes['posed_freev'][frame_number]
                pcd = self._meshes['original_obj'][frame_number] 
                ps_mesh_ = ps.register_surface_mesh('overlay1', 
                                                    vertices=-1*offset + mesh.verts.cpu().numpy() - [0.,np.min(mesh.verts.cpu().numpy()[:,1]), 0.], 
                                                    faces=mesh.faces.cpu().numpy())
                ps_mesh_ = ps.register_point_cloud('overlay2', 
                                                    points=-1*offset + pcd.verts.cpu().numpy() - [0.,np.min(mesh.verts.cpu().numpy()[:,1]), 0.], 
                                                    point_render_mode='sphere',
                                                    radius = 0.0008)
                
                
                # Already booted
                onBoot = False
        
        # Set callbacks
        ps.set_user_callback(general_callback)     
                
    def callback(self,)->None:
                
        def general_callback():
            global frame_number, checkbox, onBoot
                        
            psim.TextUnformatted("Frame Control")
            psim.Separator()
            
            # Offset
            offset = np.array([1.0,0.0,0.0])
            
            # Add Participant selection on startup
            global comboBox
            psim.PushItemWidth(200)
            changed = psim.BeginCombo("Actively selected participant", self._participants[comboBox])
            if changed:
                for i,val in enumerate(self._participants):
                    _, selected = psim.Selectable(val, comboBox==i)
                    if selected:
                        comboBox = i
                psim.EndCombo()
            psim.PopItemWidth()
                
            if(psim.Button("Run") or onBoot):
                # This code is executed when the button is pressed
                print("Parsing dataset")
                self.jit_parse_dataset()
                
                ####################################
                # Reparse the dataset and run app compute
                # See how much mem is needed to load all meshes
                # If not good redo like b4: recall register frame each time
                print(f"Parsing Dataset for participant {self._participants[comboBox]}")
                # Reset meshes data structure
                from collections import defaultdict
                self._meshes = defaultdict(list)
                self.parse_dataset(verbose=False)
                print("Computing error metrics")
                self.runAppCompute()
                onBoot = True
                
                # Show quartiles for participant and release handle
                self.compute_quartiles_Draw()


            # By default all meshes are shown
                    
            # Slider to choose frame
            changed, frame_number = psim.SliderInt(label="Frame number", 
                                                     v=frame_number, 
                                                     v_min=0, 
                                                     v_max=self._len-1)
            button_pressed = psim.Button("Show Frame")
            if(button_pressed or onBoot):
                # Call register frame if the meme is not suff
                for i,meshType in enumerate(list_of_meshes_to_load):
                    mesh = self._meshes[meshType][frame_number]
                    ps_mesh = ps.register_surface_mesh(checkbox[meshType], 
                                                       vertices=i*offset + mesh.verts.cpu().numpy() - [0.,np.min(mesh.verts.cpu().numpy()[:,1]), 0.], 
                                                       faces=mesh.faces.cpu().numpy())
                # Show scan to mesh with color bar
                mesh = self._meshes['posed_freev'][frame_number]
                ps_mesh = ps.register_surface_mesh('S2M Distance ', 
                                                    vertices=(i+1)*offset + mesh.verts.cpu().numpy() - [0.,np.min(mesh.verts.cpu().numpy()[:,1]), 0.], 
                                                    faces=mesh.faces.cpu().numpy())
                ps_mesh.add_scalar_quantity("s2m", 
                                            self._errors['distance_to_meanPose'][frame_number].cpu().numpy(), 
                                            enabled=True,)
                  
                # Show distances on meshes with color bar
                mesh = self._meshes['Tposed_freev'][frame_number]
                ps_mesh = ps.register_surface_mesh('Distance to mean mesh', 
                                                    vertices=(i+2)*offset + mesh.verts.cpu().numpy() - [0.,np.min(mesh.verts.cpu().numpy()[:,1]), 0.], 
                                                    faces=mesh.faces.cpu().numpy())
                ps_mesh.add_scalar_quantity("Mean distance to pose", 
                                            self._errors['distance_to_meanPose'][frame_number].cpu().numpy(), 
                                            enabled=True,)

                # Already booted
                onBoot = False
        
        # Set callbacks
        ps.set_user_callback(general_callback)     
        
    def compute_quartiles_Draw(self,):
        # TODO: add distance to mean
        fig, (ax1, ax2) = plt.subplots(2, figsize=(15, 15))
        fig.suptitle(f'Per frame Metrics for participant {self._participants[comboBox]}')
        # Draw per frame distances to mean pose as quartiles
        tmp = [x.cpu().numpy() for x in self._errors['distance_to_meanPose']]
        ax1.boxplot(tmp, showfliers=False)
        ax1.set(xlabel='Frames', ylabel='Mean distance to mean template shape in (mm)')
        ax1.set_title(f' Distance to mean mesh per frame')
        # Draw per frame distances s2m
        tmp = [x.cpu().numpy() for x in self._errors['s2m_perVertex']]
        ax2.boxplot(tmp, showfliers=False)
        ax1.set(xlabel='Frames', ylabel='Per vertex s2m distance in (mm)')
        ax2.set_title(f'S2M distance per frame') 
        img_name = os.path.join(self._data_root, self._participants[comboBox]+ "_plot.png")
        fig.savefig(img_name)
        #plt.show()
        from PIL import Image
        image = Image.open(img_name)
        image.show()
           
        
    def run(self,)-> None:
        self.callback_simple()
        ps.show()
        
    