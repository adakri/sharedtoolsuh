import argparse
from utils import *
from PolyScopeApp import *



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # data group 
    data_group = parser.add_mutually_exclusive_group()
    data_group.add_argument("--data_folder", "--data_folder", type=str, help="Path to folder containing data to compare.")
    parser.add_argument("--simple", action='store_true', help="Simple visualisation, no compute")
   
    args = parser.parse_args()
    # Check 
    try:
        assert os.path.exists(args.data_folder)
    except Exception:
        print("The folder does not exist, please check the path!")
        sys.exit(1)
    app = psAPP(args.data_folder, args.simple)
    app.run()
    exit()
    app = miniApp(args.data_folder)
    app.runApp()