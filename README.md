# SharedToolsUH


This repository contains the tools that were shared with external collaborators as part of the ShapeUp! Keiki project. In order to keep dependancies minimal (and seperate from those of the ShapeUp main repo and the amalthea repo), these tools are made into a seperate project.

---------------------------------------------------------------------------------------------------------------------
## Decolourization tool :hammer_and_wrench:

A simple, self contained tool in python to remove colours from scans outputted by a given recontruction software.
 <details>
  <summary>Details</summary>

### Installation
This code is self contained, you should only need the python interpreter, and should work in both windows and Linux. If you don't have python in your machine, please folow [these instructions](https://www.python.org/downloads/) to download it.

### Running the script 
In order to run the script on a folder containing a series of obj files

```shell
python scripts/remove_colour_from_agi_mesh.py -fp path_to_folder [-o path_to_output]
```
or run it on a single file:
```shell
python scripts/remove_colour_from_agi_mesh.py -sp path_to_folder/mesh.obj [-o path_to_output]
```
@ For PBRC: You can try and run it on the mini Dome phantom sample tht's in `resources/scan_data` using the following command:
```shell
python scripts/remove_colour_from_agi_mesh.py -fp resources/scan_data
```


The results in the defult folder ` ./decolourized_ouput` should look like this:

Input scan             |  Decolourized output
:-------------------------:|:-------------------------:
![](resources/phantom00.png)  |  ![](resources/phantom01.png)


</details>

---------------------------------------------------------------------------------------------------------------------
## Mesh registration interactive viewer :goggles:

An interactive tool to visualize the scans, registrations and distances. This used as a screening tool for the amalthea tool.

 <details>
  <summary>Details</summary>

### Prelimenary setUp
We use conda as apackaging tools for all the python projects that are done in this repository, please follow [this exhaustive installation guide](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) to properly install conda on your system of choice. 

### Evaluation tool
A simple and rudimentary app to evaluate the results of the pipeline registration. The tool runs both in command line and in a jupyter notebook. We recommend the use of the jupter notebook (More stable ipywidgets rednering, no issues with different browsers/vpns).

Before starting, set up your conda environement by running:
```bash
bash Evaluator/init.sh
```
This app was tested on an Ubunut 22.04 using CUDA 11.8 (RTX A6000) and requires a GPU for the computation part. 


There are two ways of running the app:
#### Integrated viewer:
Is the most stable version, you can run it through the following command line:

```bash
(sharedTools) $ python Evaluator/main.py --data_folder dataSetRoot/
```
The mesh folders should have the same layout as the pipeline outputs (and as such the uploads), which are for reference:
```bash
dataSetRoot/
        ├── path/
        |   ├── frame&participantReference
        |   │   ├── mesh_0_scaled.ply 
        |   │   └── img_0_freev.obj
        |   │   └── img_0_reposed_t.obj
        |   │   └── img_0.obj
        |   │   └── tpose_img_0.obj
```
When run on one of the uploads (results of the pipeline), the UI looks like this:

<img src="./resources/app1.png"  width="600" height="300">
<img src="./resources/app2.png"  width="600" height="300">





#### Web based solution (beta):

:warning: This solution is adapted for remote environments where window forwarding is not possible. It's memory footprint and overall peformance is very undefined, it is provided as a base for anyone who is forced to use a fully offline web based viewing solution. The use of the integrated viewer is however very advised.

In order to run the app through a jupyter notebook, please refer to the [notebook file](https://gitlab.inria.fr/shape-up-keiki/sharedtoolsuh/-/blob/main/Evaluator/main.ipynb). To run it through command line on folder that are in the path ***path/***:

```bash
(sharedTools) $ python Evaluator/main.py --data_folder path/
```
The mesh folders should have the same layout as the pipeline outputs (and as such the uploads), which are for reference:
```bash
path/
├── frame&participantReference
│   ├── mesh_0_scaled.ply 
│   └── img_0_freev.obj
│   └── img_0_reposed_t.obj
│   └── img_0.obj
│   └── tpose_img_0.obj
```

#### pointcloudViz branch:
This branch is dedicated to simple visuale dataset evaluation, that does not invole any computation. It was designed specifically for the MRI registration in the adipose dataset registrations.

```bash
(sharedTools) $ python Evaluator/main.py --data_folder dataSetRoot/ --simple
```
The mesh folders should have the same layout as the pipeline outputs (and as such the uploads), which are for reference:
```bash
dataSetRoot/
├── frame&participantReference
│   ├── mesh_0_scaled.ply <-- Point cloud 
│   └── img_0_freev.obj
│   └── img_0_reposed_t.obj
│   └── img_0.obj
│   └── tpose_img_0.obj
```
<img src="./resources/app3.png"  width="600" height="300">

</details>