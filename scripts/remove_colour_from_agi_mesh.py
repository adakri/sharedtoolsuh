# Copyright 2024 adakri
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# In py std lib
import argparse
import os

def remove_vertex_colors(input_file, output_file):
    with open(input_file, 'r') as f:
        lines = f.readlines()
    f.close()
    # Openoutput file
    with open(output_file, 'w') as f:
        for line in lines:
            if not line.startswith('v '):
                # Write the line as it is if it's not a vertex line
                f.write(line)
            else:
                # Remove color information from vertex line
                vertex_data = line.split()
                if len(vertex_data) > 6:
                    # Remove the color information
                    vertex_data = vertex_data[:6]
                f.write(' '.join(vertex_data) + '\n')


def main(list_of_paths, input_folder_path, out_folder):
    for path in list_of_paths:
        print(f"Decolourizing mesh in {path}")
        remove_vertex_colors(input_file=os.path.join(input_folder_path, path),
                             output_file=os.path.join(out_folder, path))
        

if __name__ == "__main__":
    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')
        
    parser = argparse.ArgumentParser()
    parser.add_argument("--scan_path", "-sp", type=str, required=False, help="path to a single target scan")
    parser.add_argument("--folder_path", "-fp", type=str, required=False, help='path to a folder of scans, if this arg is used the first is going to be ignored')
    parser.add_argument("--out_path", "-o", type=str, required=False, help='Where to put the outputs', default="./decolourized_ouput")
    
    args = parser.parse_args()
    # Parse the folder or select the single file
    list_of_scan_paths = []
    if(args.scan_path is not None):
        assert os.path.exists(args.scan_path), f"The scan path {args.scan_path} does not exist"
        list_of_scan_paths = [os.path.basename(args.scan_path)]
        args.folder_path = os.path.dirname(args.scan_path)
    elif(args.folder_path is not None):
        assert os.path.exists(args.folder_path), f"The scan path {args.folder_path} does not exist"
        list_of_scan_paths = sorted(os.listdir(args.folder_path))
        print(f"Processing fodler {args.folder_path} with {len(list_of_scan_paths)} scans")
    # Mke output path
    os.makedirs(args.out_path, exist_ok=True)
    # Run the decolourization
    main(list_of_paths=list_of_scan_paths, input_folder_path=args.folder_path, out_folder=args.out_path) 
    print(f"Done! all files were saved with the same names in {args.out_path}")