import numpy as np
import sys,os 
from pytorch3d.io import load_objs_as_meshes
import matplotlib.pyplot as plt
import torch
from dataclasses import dataclass


device = 'cuda:0'

@dataclass
class obj:
    """Struct for Objs 
    """
    verts: torch.tensor
    faces: torch.tensor

def load_mesh(fileName: str,
                  device='cpu',
                  verbose=True)-> obj:
        """Load PLY or OBJ meshes

        Args:
            fileName (str): Path to file to open
            device (str, optional): Device of computation. Defaults to 'cpu'.

        Returns:
            obj: _description_
        """
        import warnings
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            from pytorch3d.io import IO
            if(fileName[-3:] == 'obj'):
                try:
                    # Try texture reading with file
                    mesh = load_objs_as_meshes(
                        [fileName], device=device
                    )  # TODO: add warning here
                    vertices = mesh.verts_packed().to(device)
                    faces = mesh.faces_packed().to(device)
                except Exception as e:
                    print("Failed to read file. Reason: %s" % (e))
                    sys.exit(1)
            elif(fileName[-3:] == 'ply'):
                from pytorch3d.io import IO
                mesh = IO().load_mesh(fileName, device=device)
                vertices = mesh.verts_packed().to(device)
                faces = mesh.faces_packed().to(device)
            else:
                print(f"Format {fileName[-3:]} is unkown")
        if(verbose):
            print("Successfly loaded mesh!")
        return obj(verts=vertices, faces=faces)

if __name__ == "__main__":
    base_name = "/home/adakri/AdiposeTissueRegistration/data/adipose_nnstd_pose"
    results_name = "/home/adakri/AdiposeTissueRegistration/results/views"
    # Parse dataset
    #for participant in sorted(os.listdir(base_name)):
    participant = 'AdiposeTissue_181123_03419_AK'
    subresult = os.path.join(base_name, participant)
    for poses in os.listdir(subresult):
        template_fn = os.path.join(os.path.join(subresult, poses), 'img_0_freev.obj')
        scan_fn = os.path.join(os.path.join(subresult, poses), 'mesh_0_scaled.ply')
        assert os.path.exists(template_fn) and os.path.exists(scan_fn)
        template = load_mesh(template_fn)
        scan = load_mesh(scan_fn)
        # Compute distance in torch
        from pytorch3d.loss.point_mesh_distance import point_face_distance
        from pytorch3d.structures import Meshes, Pointclouds
        
        pcls = Pointclouds(points=template.verts.clone().detach().to(device=device, dtype=torch.float32).unsqueeze(dim=0))
        meshes = Meshes(verts=scan.verts.clone().detach().to(device=device, dtype=torch.float32).unsqueeze(dim=0),
                        faces=scan.faces.clone().detach().to(device=device, dtype=int).unsqueeze(dim=0))
        # packed representation for pointclouds
        points = pcls.points_packed() # (P, 3)
        points_first_idx = pcls.cloud_to_packed_first_idx()
        max_points = pcls.num_points_per_cloud().max().item()

        # packed representation for faces
        verts_packed = meshes.verts_packed()
        faces_packed = meshes.faces_packed()
        tris = verts_packed[faces_packed]  # (T, 3, 3)
        tris_first_idx = meshes.mesh_to_faces_packed_first_idx()
        max_tris = meshes.num_faces_per_mesh().max().item()

        # point to face distance: shape (P,)
        point_to_face = point_face_distance(
            points, points_first_idx, tris, tris_first_idx, max_points
        )

        diff = point_to_face # mesh to scan
        print(torch.mean(diff), torch.mean(diff))
        merr = diff.cpu().numpy()

        # Render color map
        from psbody.mesh import MeshViewer, MeshViewers, Mesh
        mesh = Mesh(filename=template_fn)
        mvs = MeshViewers((1,2))# create a viewer
        
        # Show error
        mv = mvs[0][0]
        mv.set_background_color(np.array([1.0,1,1])) # set bg color to white
        max_err=1*1e-4
        mesh.set_vertex_colors_from_weights(merr/max_err, scale_to_range_1=False)
        mv.set_static_meshes([mesh]) # Add the mesh to the viewer
        
        #mv.save_snapshot('/home/adakri/AdiposeTissueRegistration/sharedtoolsuh/scripts/img.png', blocking=True) # Save the figure as an image file
        # Show mesh
        # Show error
        mv = mvs[0][1]
        mv.set_background_color(np.array([1.0,1,1])) # set bg color to white
        mesh = Mesh(filename=scan_fn)
        mv.set_static_meshes([mesh])
        #sys.exit(0)